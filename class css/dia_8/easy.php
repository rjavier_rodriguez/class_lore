<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Easy Layout</title>
	<link rel="stylesheet" href="easy.css">
</head>
<body>

	<div class="wrapper">
		<header>
			<h1>Soy el header</h1>
		</header>
		<main>
			<h2>Main Conten</h2>	
		</main>

		<aside>
			<h2>Aside</h2>
		</aside>

		<footer>
			<h2>Footer</h2>
		</footer>
		
	</div>
	
</body>
</html>