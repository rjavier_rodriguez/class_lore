<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Add new User</title>
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<style>
		.container{
			width: 500px;
			height: auto;
			

		}
		table{
			text-align: center;

		}
		thead{
			font-size: 2em;
			font-weight: bold;
		}
		.form-group{
			padding: 20px;
		}
		#button{
			margin-bottom:20px;
			margin-left:365px;


		}
	</style>
</head>
<body>
	<article class="container">
		
		<header class="page-heade">
			<h1>Lavarel 4.2<small>Rjavier</small></h1>
		</header>
			
		
	
	
		<div class="panel panel-default">
  		<!-- Default panel contents -->
  		<div class="panel-heading">Add new user</div>

  <!-- Form -->
  	<form role="form" action="store">
	  <div class="form-group">
	    <label>Name:</label>
	    <input type="text" name="name" class="form-control" id="name" placeholder="Enter your name">
	  </div>
	  <div class="form-group">
	    <label>Email</label>
	    <input type="email" class="form-control" name="email" id="email" placeholder="Enter your email">
	  </div>
	 
	  <button type="submit" id="button" class="btn btn-default">Add user</button>
	</form>

	@if(Session::has('message'))
		<div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
	@endif
			  
	</article>
</body>
</html>