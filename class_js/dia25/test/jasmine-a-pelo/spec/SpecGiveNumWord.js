
describe("giveNumWord", function() {
    it("Return the word number into a paragraph:", function() {
        expect(giveNumWord("Lorem ipsum dolor sit amet, consectetur adipisicing elit.", "consectetur")).toEqual("La palabra consectetur es la numero: 6");
    });
});
