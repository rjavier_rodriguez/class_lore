isSubtring("ro","roma");

describe("isSubtring", function() {
    it("Return true if string 1 are indexof the string 2", function() {
        expect(isSubtring("ro","roma")).toEqual(true);
    });
});
