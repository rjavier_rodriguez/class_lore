
describe("replaceSpace", function() {
    it("Return string with change  the whitespace for a %20", function() {
        expect(replaceSpace("hola hola hola")).toEqual("hola%20hola%20hola");
    });
});
