describe("giveUnicode", function() {
    it("Return the unicode for one letter in the string:", function() {
        expect(giveUnicode("javier")).toEqual([106, 97, 118, 105, 101, 114]);
    });
});
